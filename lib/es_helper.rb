# frozen_string_literal: true

module EsHelper
  def as_indexed_json(_params = {})
    {
      title: title,
      description: description,
      searching: searching
    }
  end

  def preview
    if description.size > 25
      description[0, 25] + '...'
    else
      description
    end
  end
end
