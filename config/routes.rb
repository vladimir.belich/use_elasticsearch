Rails.application.routes.draw do
  get 'search', to: 'home#search'
  resources :article, :blog_post, :news
end
