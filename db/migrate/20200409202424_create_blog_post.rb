class CreateBlogPost < ActiveRecord::Migration[6.0]
  def change
    create_table :blog_posts do |t|
      t.string :title
      t.string :description
      t.boolean :searching, default: false
      t.timestamps null: false
    end
  end
end
