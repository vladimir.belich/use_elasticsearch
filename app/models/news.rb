class News < ApplicationRecord
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  include EsHelper
  include ElasticMyAnalyzer
  
  settings ES_SETTING do
    mappings dynamic: 'true' do
      indexes :title, type: 'text', analyzer: 'my_analyzer'
      indexes :description, type: 'text', analyzer: 'my_analyzer'
      indexes :searching, type: 'boolean'
    end
  end
end
