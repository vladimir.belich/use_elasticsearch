module ApplicationHelper
  def search_result_link(result)
    case result[:record_type]
    when 'BlogPost'
      blog_post_path(result[:record_id])
    when 'Article'
      article_path(result[:record_id])
    when 'News'
      news_path(result[:record_id])
    end
  end
end
